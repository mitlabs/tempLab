#include <stdio.h>
#include <stdlib.h>

typedef struct edge {
    int source, destination, weight;
    struct edge * next;
} Edge;

Edge * createEdge(int src, int dest, int weight) {
    Edge * new = (Edge *) malloc(sizeof(Edge));
    new->source = src;
    new->destination = dest;
    new->weight = weight;
    return new;
}

void insertEdge(Edge ** list, Edge * new) {
    Edge * iter = (Edge *) malloc(sizeof(Edge));
    iter = *list;
    Edge * prev = NULL;
    if (iter == NULL) {
        *list = new;
        return;
    }
    while (iter->weight < new->weight) {
        prev = iter;
        iter = iter->next;    
    }

    if (prev == NULL) {
        new->next = *list;
        *list = new;
    }
    else {
        if (iter == NULL) 
            prev->next = new;
        else {
            new->next = prev->next;
            prev->next = new;
        }
    }
}

void kruskal() {

}

void main() {

    int i, j, n;
    printf("Enter number of vertices ");
    scanf("%d",&n);

    int ** costMat = (int **) malloc(n * sizeof(int *));
    printf("Enter the cost matrix\n");
    for(i = 0; i < n; i++) {
        costMat[i] = (int *) calloc(n,sizeof(int));
        for(j = 0; j < n; j++) {
            printf("Enter a[%d][%d] ",i,j);
            scanf("%d",&costMat[i][j]);
        }
    }

}